.MODEL SMALL
.CODE
ORG 100h
satu:
	jmp dua
	kal1	DB 'saya lagi belajar'
		DB 'bahasa assembly'
	kal2	DB 'ternyata'
		DB 'asik'
		DB '$'
dua:
	mov AH, 09h
	mov DX, offset kal1
	int 21h
	mov DX, offset kal2 
	int 21h
	int 20h
end satu
